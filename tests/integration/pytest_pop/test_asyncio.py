import pytest


@pytest.mark.asyncio
async def test_asyncio(hub, event_loop):
    assert hub.pop.Loop is event_loop
