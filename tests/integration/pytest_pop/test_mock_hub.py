import pytest


@pytest.fixture
def hub(hub):
    hub.pop.sub.add(dyne_name="foo")
    hub.foo.func = lambda x: x
    yield hub


def test_autospec(hub, mock_hub):
    val = "12345"
    # Call a function on the mock hub
    mock_hub.foo.func(val)

    # Verify that the function was called with the given parameter
    mock_hub.foo.func.assert_called_once_with(val)
